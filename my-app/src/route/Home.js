import React, { PureComponent } from 'react';
import Firstpage from '../page/Home/Firstpage';
import Pagetitle from '../components/texts/Pagetitle';
import Headercenter from '../components/header/Header_center';


const title = 'Crée ton programme';

export default class Home extends PureComponent {
  render() {
    return (
      <div >
        <Headercenter></Headercenter>
        <div class="margin_top">
        <Pagetitle title={title}></Pagetitle>
        <Firstpage></Firstpage>
        </div>
      </div>
    )
  }
}
