import React, { PureComponent } from 'react';
import Header from '../components/header/Header';
import Pagetitle from '../components/texts/Pagetitle';
import Group from '../page/Group/Group';

const titre = 'Rentre tes dates !';


export default class Home extends PureComponent {
  render() {
    return (
      <div>
        <Header></Header>
        <Pagetitle title={titre}></Pagetitle>
        <Group></Group>
      </div>
    )
  }
}
