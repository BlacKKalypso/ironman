import React, { PureComponent } from 'react';
import Header from '../components/header/Header';
import Pagetitle from '../components/texts/Pagetitle';
import Validation from '../page/Validation/Validation';

const titre = 'Super !';


export default class Home extends PureComponent {
  render() {
    return (
      <div>
        <Header></Header>
        <Pagetitle title={titre}></Pagetitle>
        <Validation></Validation>
      </div>
    )
  }
}
