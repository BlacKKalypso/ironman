import React, { PureComponent } from 'react';
import Header from '../components/header/Header';
import Pagetitle from '../components/texts/Pagetitle';
import Chargement from '../page/Chargement/Chargement';

const titre = 'Ton programme arrive !';


export default class Home extends PureComponent {
  render() {
    return (
      <div>
        <Header></Header>
        <Pagetitle title={titre}></Pagetitle>
        <Chargement></Chargement>
      </div>
    )
  }
}
