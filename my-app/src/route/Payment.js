import React, { PureComponent } from 'react';
import Header from '../components/header/Header';
import Pagetitle from '../components/texts/Pagetitle';
import Payment from '../page/Payment/Payment';

const titre = 'On y est presque ...';


export default class Home extends PureComponent {
  render() {
    return (
      <div>
        <Header></Header>
        <Pagetitle title={titre}></Pagetitle>
        <Payment></Payment>
      </div>
    )
  }
}
