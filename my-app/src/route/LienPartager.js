import React, { PureComponent } from 'react';
import Header from '../components/header/Header';
import Pagetitle from '../components/texts/Pagetitle';
import LienPartage from '../page/LienPartager/LienPartager';

const titre = 'Partage le lien';


export default class Home extends PureComponent {
  render() {
    return (
      <div>
        <Header></Header>
        <Pagetitle title={titre}></Pagetitle>
        <LienPartage></LienPartage>
      </div>
    )
  }
}
