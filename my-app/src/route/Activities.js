import React, { PureComponent } from 'react';
import Header from '../components/header/Header';
import Pagetitle from '../components/texts/Pagetitle';
import Activities from '../page/Activities/Activities';
import Primarybutton from '../components/buttons/Primarybutton';
import Price from '../page/Activities/Price';
import Loading from '../page/Summary/Loading';

const titre = 'Ton programme';
const style = {
  textAlign: 'center',
}

const link = true;
const href = '/Summary';
const msg = 'Valider';
const color = '#fafafa';

export default class Home extends PureComponent {
  render() {
    return (
      <div>
        <Header></Header>
        <Loading></Loading>
        <div style={style}>
          <Pagetitle title={titre}></Pagetitle>
          <div className="contain_fixed">
            <Activities></Activities>
            <Price></Price>
            <Primarybutton
              link={link}
              href={href}
              msg={msg}
              color={color}
            />
          </div>
        </div>
      </div>
    )
  }
}
