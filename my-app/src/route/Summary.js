import React, { PureComponent } from 'react';
import Header from '../components/header/Header';
import Summary from '../page/Summary/Summary';
import Pagetitle from '../../src/components/texts/Pagetitle';
import Primarybutton from '../../src/components/buttons/Primarybutton';
import Secondarybutton from '../../src/components/buttons/Secondarybutton';
import Loading from '../page/Summary/Loading';


const title = 'On récapitule';

const link = true;
const href = '/Activities';
const msg = 'Modifier';
const color = '#c7001e';

const msg2 = 'Demande aux autres participants';
const href2 = '/Lien';

const msg3 = 'Réserve seul';
const href3 = '/Connexion';

export default class Home extends PureComponent {
  render() {
    return (
      <div>
        <Header></Header>
        <Pagetitle title={title}></Pagetitle>
        <div className="contain_fixed_recap">

          <Summary></Summary>
        </div>
        <div className="summary-buttons ">
          <Primarybutton link={link} msg={msg} href={href} color={color}>
          </Primarybutton>
          <div className="button_small">
            <Secondarybutton link={link} msg={msg2} href={href2} color={color}>
            </Secondarybutton>
          </div>
          <Primarybutton link={link} msg={msg3} href={href3} color={color}>
          </Primarybutton>
        </div>
        </div>
        )
      }
    }
