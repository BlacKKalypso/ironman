import React, { PureComponent } from 'react';
import Header from '../components/header/Header';
import Pagetitle from '../components/texts/Pagetitle';
import SelectPayment from '../page/SelectPayment/SelectPayment';

const titre = 'on s\'arrange comment?';


export default class Home extends PureComponent {
  render() {
    return (
      <div>
        <Header></Header>
        <Pagetitle title={titre}></Pagetitle>
        <SelectPayment></SelectPayment>
      </div>
    )
  }
}
