import React, { PureComponent } from 'react';
import Header from '../components/header/Header';
import Swipecards from '../page/Cards/swipecard/Swipecards';
import Pagetitle from '../components/texts/Pagetitle';

const title = 'Swipe tes envies';

export default class Home extends PureComponent {
  render() {
    return (
      <div>
        <Header></Header>
        <div className="container_page">
        <Pagetitle title={title}></Pagetitle>
        <Swipecards></Swipecards>
        </div>
      </div>
    )
  }
}
