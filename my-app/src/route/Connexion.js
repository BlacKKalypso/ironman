import React, { PureComponent } from 'react';
import Header from '../components/header/Header';
import Pagetitle from '../components/texts/Pagetitle';
import Connexion from '../page/Connexion/Connexion';


const title = 'Connecte toi';

export default class Home extends PureComponent {
  render() {
    return (
      <div>
        <Header></Header>
        <Pagetitle title={title}></Pagetitle>
        <Connexion></Connexion>
      </div>
    )
  }
}
