import React, { PureComponent } from 'react';
import Header from '../components/header/Header';
import Pagetitle from '../components/texts/Pagetitle';
import Inscription from '../page/Inscription/Inscription';

const titre = 'Inscris toi';


export default class Home extends PureComponent {
  render() {
    return (
      <div>
        <Header></Header>
        <Pagetitle title={titre}></Pagetitle>
        <Inscription></Inscription>
      </div>
    )
  }
}
