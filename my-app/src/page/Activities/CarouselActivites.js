import React from 'react';
import liste from './liste.json';
import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";
import "./alice-carousel.css";


class Gallery extends React.Component {

  activities = this.props.activities;
  choice = this.props.choice;

  state = {
    currentIndex: 0,
    responsive: { 1024: { items: 1 } },
    galleryItems: this.galleryItems(),
  }
 
  slideTo = (i) => this.setState({ currentIndex: i })
 
  onSlideChanged = (e) => this.setState({ currentIndex: e.item })
 
  slideNext = () => this.setState({ currentIndex: this.state.currentIndex + 1 });
 
  slidePrev = () => this.setState({ currentIndex: this.state.currentIndex - 1 })
 
  thumbItem = (item, i) => <span onClick={() => this.slideTo(i)}>* </span>

  galleryItems() {

    const handleClick = (id) => {
      liste[id].selected = !liste[id].selected;
      console.log(liste[id].selected);

      var element = document.getElementsByClassName(id);
      console.log(element)
      

      for(let i = 0; i < element.length; i++){
        if (element[i].classList.contains('selected')){
          element[i].classList.remove('selected');
        } else {
        element[i].classList.add('selected');
        };
      }
      
      for(let i=0; i<this.choice.length; i++)
      {
        let toCheck = this.choice[i];
        if(toCheck===id){
          this.choice.splice(i, 1);
          localStorage.setItem('activity', JSON.stringify(this.choice));
          return 0;
        } 
      }
      this.choice.push(id);
      localStorage.setItem('activity', JSON.stringify(this.choice));
    }

    return this.activities.map((i, j) =>
      <div onClick={handleClick.bind(this, liste[i].id)} id={liste[i].id} className={'activity ' + liste[i].id} key={liste[i].id}>
        <div className="activity-carousel-hover">{liste[i].titre}</div>
        <img alt="activité" src={liste[i].img}/>
      </div>
      )
  }
 
  render() {
    const { galleryItems, responsive, currentIndex } = this.state
    return (
      <div className="alice-carousel">
        <AliceCarousel
          dotsDisabled={true}
          buttonsDisabled={true}
          items={galleryItems}
          responsive={responsive}
          slideToIndex={currentIndex}
          onSlideChanged={this.onSlideChanged}
        />
        <button className="button-carousel button-carousel-left" onClick={() => this.slidePrev()}></button>
        <button className="button-carousel button-carousel-right" onClick={() => this.slideNext()}></button>
      </div>
    )
  }
}

export default Gallery;