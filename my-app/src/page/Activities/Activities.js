import React, { Component } from 'react';
import activities from './activities.json';
// import liste from './liste.json';
// import Listactivities from './Listactivities.js';
import './activities.css'
import Gallery from './CarouselActivites.js';
//import Price from './Price';

export default class Activities extends Component {
  constructor() {
    super();
    this.state = {
      myActivities: '',
      myChoice:[],
      Selected:[]
    }
  }

  componentWillMount(){
    this.setState({myChoice: []});
    localStorage.removeItem('myChoice');
    const myCards = JSON.parse(localStorage.getItem('myCards'));
    const myList = activities.cards
    const myCardsChoice = myList[0].filter((activity, index) => myCards.includes(index));
    console.log('mes cartes', myCardsChoice);
    const setIndex = localStorage.getItem('index');
    this.setState({
      myActivities: myCardsChoice,
      index: setIndex,
    });
  }

  handleClick = (id) => {
    const stateChoice = this.state.myChoice;
    let idtocheck = id;
    let index = stateChoice.indexOf(idtocheck);

    if (stateChoice.includes(idtocheck)){
      stateChoice.splice(index, 1);
    } else {
      stateChoice.push(idtocheck);
    }
    stateChoice.sort((a, b) => a - b)
    console.log('stateChoice: ', stateChoice)
    localStorage.setItem('myChoice', JSON.stringify(this.state.myChoice));
  }


  render() {
    const myListStyle = {
      textAlign: 'left',
      color:'#fafafa',
      fontWeight:'bold',
      fontSize: '30px',
      marginBottom: '20px',
      marginTop: '10px',
      
    }

    const myActivitiesListStyle = {
      textAlign: 'left',
      color:'#fafafa',
      textShadow: 'black 0.1em 0.1em 0.2em',
      fontWeight:'bold',
      fontSize: '30px',
      marginBottom: '20px',
      marginTop: '10px',
      height: '300px',
    }

    const choice = [];

    return (
      this.state.myActivities.map((activity, index) =>
      <div className="wrapper activities" key={index} style={myListStyle}>
      
        <div className="activities__content"> 
            {activity.titre} 
          <div className="activities__content__list" style={myActivitiesListStyle}>
            <Gallery choice={choice} activities={activity.activities} activityIndex={index}></Gallery>
          </div>
        </div>
      </div>
    )
    );
  }
}
