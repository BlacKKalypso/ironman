import React, { PureComponent } from 'react'
import './lienPartager.css';
import { CopyToClipboard } from 'react-copy-to-clipboard'
import Primarybutton from '../../components/buttons/Primarybutton';

const link = true;
const href = '/Summary';
const msg = 'retour';
const color = '#c7001e';

const href2 = '/selectPayment';
const msg2 = 'continuer';

export default class Group extends PureComponent {

    state = {
        message: 'https://bit.ly/go1',
        copied: false,
    };

    render() {
        return (
            <div className="page_content link_page">
                <div className="link_shared">

                    <CopyToClipboard text={this.state.message}>
                        <input type="text" ref="input" value={this.state.message}></input>
                    </CopyToClipboard>
                </div>
                <div className="buttons_center">
                        <Primarybutton link={link} msg={msg} href={href} color={color}>
                        </Primarybutton>

                        <Primarybutton link={link} msg={msg2} href={href2} color={color}>
                        </Primarybutton>
                    
                </div>
                <div className="description">
                    <p>Partage ce lien avec les autres participants afin de valider ton programme</p></div>

            </div>
        );
    }
}


