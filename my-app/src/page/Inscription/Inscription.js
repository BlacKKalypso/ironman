import React, { Component } from 'react';
import '../Connexion/Connexion';
import Primarybutton from '../../components/buttons/Primarybutton';

const link = true;
const href = '/Summary';
const msg = 'GO !';
const color = '#c7001e';

export default class Connexion extends Component {
    render() {
        return (
            <div>
                <div className="page_content">
                    <form method="post" action={href} >
                        <div className="connexion-form">
                            <h2>Pour obtenir ton programme</h2>
                            <div className="data_register">
                                <input type="text" name="fullname" id="fullname-field" placeholder="Nom / prénom" required />
                                <input type="text" name="birthday-date" id="birthday-date-field" placeholder="date de naissance" required />
                                <input type="email" name="email" id="email-field" placeholder="E-mail" required />
                                <input type="text" name="country" id="country-field" placeholder="Pays" required />
                                <input type="text" name="state" id="state-field" placeholder="Canton" required />
                            </div>

                            <div className="buttons_center">
                                <div className="button-go">
                                    <Primarybutton link={link} msg={msg} href={href} color={color}>
                                    </Primarybutton>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div >
        );
    }
};