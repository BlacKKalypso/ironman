import React, { Component } from 'react';
import './selectPayment.css';
import Secondarybutton from '../../components/buttons/Secondarybutton';
import Primarybutton from '../../components/buttons/Primarybutton';


const link = true;
const href = '/Payment';
const msg = 'Payer la totalité';
const color = '#c7001e';

const msg2 = 'Partager le paiement';


export default class Connexion extends Component {
    render() {
        return (
            <div>
                <div className="page_content">
                    <div className="button-centered">
                        <div className="buttons_column">

                            <div className="button_first">
                                <Primarybutton link={link} msg={msg} href={href} color={color}>
                                </Primarybutton>
                            </div>

                            <div className="button_second">
                                <Secondarybutton link={link} msg={msg2} href={href} color={color}>
                                </Secondarybutton>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
};