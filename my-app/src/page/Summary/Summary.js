import React, { Component } from 'react';
import liste from '../Activities/liste.json';
import './summary.css'

export default class Summary extends Component {
  constructor() {
    super();
    this.state = {
      value: ''
    }
  }

  componentDidMount(){
    const activities = JSON.parse(localStorage.getItem('activity'));
    const activitiesChosed = liste.filter((item, index) => activities.includes(index));
    console.log('choosed: ', activitiesChosed)
    this.setState({activities: activitiesChosed});
  }

  handleClick = () => {
    this.setState({
      value: this.state.value
    })
    localStorage.setItem('nomSejour', this.state.value)
    console.log('click')
  }

  handleChange = (event) => {
    this.setState({value: event.target.value});
  }

  renderListe() {
    if (this.state.activities == null){
      return(
        <div>loading...</div>
      )
    }
    else{
    return (
      this.state.activities.map((activity, index) =>
      <div className="summary summaryactivity" key={index}>
        <figure>
          <img  width="200px" height="150px" alt="test" src={activity.img} />
          <div className="hover">
            <p>{activity.titre}</p>
            <p>{activity.description}</p>
          </div>
        </figure>
      </div>
    ))}}


  render() {
    return (
      <div>
      <div className="summary-container">
        {this.renderListe()}
      </div>
      <div className="nom_sejour">
      <div className="labels">
          <label for="name_journey-field" >Nom du Séjour : </label>
      </div>
      <input type="text" name="name_journey" id="name_journey-field" maxLength="30" value={this.state.value} placeholder="Le séjour du siècle" onChange={this.handleChange}/>
      <button className="primarybutton summarybutton" style={{marginLeft: '10px'}} onClick={this.handleClick}>OK</button>
  </div>
  </div>
      )
}
}

