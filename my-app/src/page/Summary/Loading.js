import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './loading.css'

export default class Loading extends Component {
    static propTypes = {
        prop: PropTypes
    }

    render() {
        return (
            
            <div className="loading_wrapper">
                <div className="title_loading">Ton programme arrive</div>
                <div className="loading_page"/>
            </div>
           
        )
    }
}
