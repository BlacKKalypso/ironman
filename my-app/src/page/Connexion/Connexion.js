import React, { Component } from 'react';
import './connexion.css';
import Secondarybutton from '../../components/buttons/Secondarybutton';
import Primarybutton from '../../components/buttons/Primarybutton';
import facebook from '../../../src/img/facebook.svg';
import google from '../../../src/img/google.svg';



const link = true;
const href = '/selectPayment';
const msg = 'GO!';
const color = '#c7001e';

const link2 = true;
const msg2 = 'Inscription';
const href2 = '/Inscription';



export default class Connexion extends Component {
    handleClick = () => {
        window.open('https://accounts.google.com/ServiceLogin/signinchooser', 'popup', 'width=600,height=600');
    }

    render() {
        return (
            <div>
                <div className="page_content">
                    <form method="post" action={href} Validate>
                        <div className="connexion-form">
                            <h2>Pour obtenir ton programme</h2>
                            <div className="social_icons_bloc">
                                <figure>
                                    <img src={facebook} width="100px" height="100px" alt="facebook" />
                                </figure>
                                <p>ou</p>
                                <figure>
                                    <a target="popup" href="https://accounts.google.com/ServiceLogin/signinchooser" onClick={this.handleClick}>
                                        <img src={google} width="100px" height="100px" alt="google" />
                                    </a>
                                </figure>
                            </div>

                            <div className="data_register">
                                <input type="email" name="email" id="email-field" placeholder="E-mail" required />
                                <input type="password" name="password" id="pw-field" placeholder="Mot de passe" required />
                            </div>

                            <div className="buttons_center">
                            
                                    <Secondarybutton link={link} msg={msg2} href={href2} color={color}>
                                    </Secondarybutton>

                    <div style={{width: '50px'}}></div>

                                            <Primarybutton link={link2} msg={msg} href={href} color={color}>
                                            </Primarybutton>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
};