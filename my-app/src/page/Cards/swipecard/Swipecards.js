import React, { Component } from 'react';
import { Card, CardWrapper } from 'react-swipeable-cards';
import './swipecards.css';
import Primarybutton from '../../../components/buttons/Primarybutton';
import right from '../../../../src/img/right-arrow.svg';
import left from '../../../../src/img/left-arrow.svg';

 

class Swipecards extends Component {
  constructor() {
    super();
    this.state = {
      wantedCards: [0,6,8,9],
      background: './cards/freerider-skiing-ski-sports-47356.png',
    }
  }

  componentWillUpdate() {
    localStorage.setItem('myCards', JSON.stringify(this.state.wantedCards));
  }

  onSwipe(data) {
    console.log(data.id + " was swiped.");
    this.setState({
      background: data.img,
    });
    console.log(data.img);
  }

  onSwipeLeft() {
    console.log("I was kicked.");
  }

  onSwipeRight(data) {
    console.log("I was swiped right.");
    const state = this.state;
    this.state.wantedCards.push(data.id);
    console.log('state after: ', state);
    localStorage.setItem('myCards', JSON.stringify(this.state.wantedCards));
  }
  renderCards() {
    const cardStyle = {
      backgroundColor: "#fafafa",
      border: 'none',
      marginTop: '120px',
      width: '300px',
      height: '350px',
      color: '#C7001E',
      fontWeight: 'bold',
      fontSize: '40px',
      textAlign: 'center',
      padding: '60px 20px',
      boxSizing: 'border-box',
      borderRadius: '8px',
      boxShadow: '0px 0px 20px -4px rgba(0,0,0,1)',
      display:'flex',
      alignItems:'center',
    }

    let data = [
      { id: 1, msg: "Sport? Toujours?", img: './cards/clubbing.png' },
      { id: 7, msg: "Un p'tit pas de danse, j'dis pas non", img: './cards/detente.png' },
      { id: 5, msg: "Besoin de détente", img: './cards/mike_horn.png' },
      { id: 2, msg: "Mike Horn, quand tu nous tiens", img: './cards/sport_extreme.png' },
      { id: 3, msg: "Un flocon de frisson", img: './cards/divertissement.png' },
      { id: 4, msg: "Divertissement c'est important", img: './cards/background2.png' }
    ];
    return data.map((d) => {
      return (
        <Card
          style={cardStyle}
          key={d.id}
          onSwipe={this.onSwipe.bind(this)}
          data={d}
          onSwipeLeft={this.onSwipeLeft.bind(this)}
          onSwipeRight={this.onSwipeRight.bind(this)}>
          {d.msg}
        </Card>
      );
    });
  }

  renderSwipe() {
    const swipeStyle = {
      width: '100%',
      display: 'flex',
      justifyContent: 'space-around',
      color: '#fafafa',
      fontSize: '40px',
      fontWeight: 'bold',
    }

    const link = true;
    const href = '/Group';
    const msg = 'suivant';
    const color = 'white';


    return (
      <div className="container_swiped" style={swipeStyle}>
        <div className="container_swiped_wrapper container_swiped_wrapper-left center ">
          <div className="align_left">
            Non, merci !
          </div>
          <figure>
            <img src={left} width="300px" alt="left-arrow" />
          </figure>
        </div>
        <div class="center-button">
        <Primarybutton
          link={link}
          msg={msg} 
          href={href}
          color={color} />
          </div>
        <div className=" container_swiped_wrapper container_swiped_wrapper-right center">
          <div className="align_right">
            Je veux !
            </div>
          <figure>
            <img src={right} width="300px" alt="right-arrow" />
          </figure>
        </div>
      </div>

    )
  }

  render() {
    const containerStyle = {
      height: '100vh',
      position: 'absolute',
      top: '0',
      backgroundImage: 'url(' + this.state.background + ')',
      backgroundSize: 'cover',
    }

    return (
      <CardWrapper style={containerStyle}>
        {this.renderCards()}
        {this.renderSwipe()}
      </CardWrapper>
    );
  }
}

export default Swipecards;