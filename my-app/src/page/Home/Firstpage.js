import React, { Component } from 'react';
import './firstpage.css';
import Primarybutton from '../../components/buttons/Primarybutton';

const link = true;
const href = '/Cards';
const msg = 'COMMENCER';
const color = '#c7001e';

export default class Firstpage extends Component {
   
  render() {
    return (
      <div className="page_content home-page">  
      <div className="subtitle">
      <h2> Ton séjour tout compris en quelques clicks !</h2></div>
        <div className="button">
          <Primarybutton link={link} msg={msg} href={href} color={color}>
          </Primarybutton>
        </div>
      </div>
    )
  }
}
