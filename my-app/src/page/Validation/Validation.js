import React, { PureComponent } from 'react'
import './validation.css';
import Validation from '../../img/validation@2x.png';




export default class Group extends PureComponent {
    componentWillMount(){
        const name = localStorage.getItem('nomSejour')
        if (name != null){
        this.setState({
            name: name
        })
    } else {
        this.setState({
            name: 'Votre séjour'
        })
    }
        console.log(name)
    }
    componentDidMount(){
        this.render()
    }
    render() {
        return (
            <div>
                <div className="page_content validation_page">
                <div className="phrase">
                <p>{this.state.name} démarre dans 16 jours !</p>
                </div>
                    <div className="validation">
                        <img src={Validation} alt="validation" height="40px" />
                    </div>
                    <div className="texte_validation">
                        <p>Un email de confirmation vous a été envoyé</p>
                    </div>
                </div>

            </div>
        );
    }
}
