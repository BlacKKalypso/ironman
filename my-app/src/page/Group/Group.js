import React, { PureComponent } from 'react'
import './group.css';
import Primarybutton from '../../components/buttons/Primarybutton';

const link = true;
const href = '/Activities';
const msg = 'GO !';
const color = '#c7001e';

export default class Group extends PureComponent {
    render() {
        return (
            <div>
                <div className="page_content text_center">
                    <form action={href}>
                    <div className="group_form">
                        <div className="left_side">
                            <div className="date_arrive">
                                <div className="subtitle">
                                    <h3>Arrivée</h3>
                                </div>
                                <div className="date">
                                    <input type="number" min="01" max="31" name="date_day" id="day-field" placeholder="jj"
                                        required />
                                    <input type="number" min="01" max="12" name="date_month" id="month-field" placeholder="mm"
                                        required />
                                    <input type="number" min="2019" max="2022" name="date_year" id="year-field"
                                        placeholder="aaaa" required />
                                </div>
                            </div>
                            <div className="number_participants">
                                <div className="subtitle">
                                    <h3>Nombre de personnes</h3>
                                </div>
                                <div className="number_select">
                                    <input id="number" type="number" step="1" min="0" max="30" placeholder="0" required />
                                </div>
                            </div>
                        </div>
                        <div className="right_side">
                            <div className="date_depart">
                                <div className="subtitle">
                                    <h3>Départ</h3>
                                </div>
                                <div className="date">
                                    <input type="number" min="01" max="31" name="date_day" id="day-field" placeholder="jj"
                                        required />
                                    <input type="number" min="01" max="12" name="date_month" id="month-field" placeholder="mm"
                                        required />
                                    <input type="number" min="2019" max="2022" name="date_year" id="year-field"
                                        placeholder="aaaa" required />
                                </div>
                            </div>
                            <div className="budget">
                                <div className="subtitle">
                                    <h3>Budget total / Groupe</h3>
                                </div>
                                <div className="budget_francs">
                                    <div className="budget_select">
                                        <input type="number" id="budget" step="50" min="0" max="20000" name="budget" placeholder="CHF" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div className="button-go">
                            <Primarybutton link={link} msg={msg} href={href} color={color}>
                            </Primarybutton>                       
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
