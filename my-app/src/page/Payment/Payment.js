import React, { PureComponent } from 'react'
import './payment.css';
import Primarybutton from '../../components/buttons/Primarybutton';
import visa from '../../img/visa.svg';
import mastercard from '../../img/mastercard.svg';
import amex from '../../img/american-express.svg';
import postfinance from '../../img/Postfinance@2x.png';
import information from '../../img/information.svg';


const link = true;
const href = '/Validation';
const msg = 'Payer';
const color = '#c7001e';

export default class Group extends PureComponent {
    render() {
        return (
            <div>
                <div className="page_content container_payment">
                    <form method="post" action={href}>
                        <div className="form_payment">

                            <div className="holder">
                                <div className="labels">
                                    <label for="cardholder"> Titulaire de la carte</label>
                                </div>
                                <input type="text" id="cardholder" name="name" required />
                            </div>

                            <div className="country">
                                <div className="labels">
                                    <label for="country-field" >Pays</label>
                                </div>
                                <input type="text" name="country" id="scountry-field" required />
                            </div>

                            <div className="n_credit_card">
                                <div className="labels">
                                    <label for="number_credit_card-field"> N° de carte</label>
                                </div>
                                <input type="text" name="number_credit_card" id="number_credit_card-field" required />
                            </div>

                            <div className="type_payment">

                                <div className="labels">
                                    <label>Modes de paiement</label>
                                </div>

                                <div className="icons_payment">

                                    <input id="visa" name="type_de_carte" type="radio" />
                                    <img src={visa} alt="visa" height="50px" width="80px" />

                                    <input id="mastercard" name="type_de_carte" type="radio" />
                                    <img src={mastercard} alt="mastercard" height="50px" width="80px" />

                                    <input id="amex" name="type_de_carte" type="radio" />
                                    <img src={amex} alt="american-express" height="50px" width="80px" />

                                    <input id="postfinance" name="type_de_carte" type="radio" />
                                    <img src={postfinance} alt="postfinance" height="50px" width="80px" />

                                </div>
                            </div>

                            <div className="date_expiration">
                                <div className="labels">
                                    <label>Date d'expiration</label>
                                </div>
                                <div className="inputs_expiration">
                                    <input type="number" name="month_exp" id="month_exp-field" min="1" max="12" required />
                                    <div className="separation"></div>
                                    <input type="number" name="year_exp" id="year_exp-field" min="2019" max="2030" required />
                                </div>
                            </div>
                            <div className="cryptogramme">
                                <div className="labels">
                                    <label>Cryptogramme visuel </label>
                                </div>
                                <div className="cryptogramme_info">
                                    <input type="number" name="number_credit_card" id="number_credit_card-field" min="100" max="9999" required />
                                    <div className="info">
                                        <img src={information} alt="information" height="20px" />
                                        <p>Le cryptogramme visuel de sécurité est un groupe de 3 ou 4 chiffres imprimés au dos de la carte de paiement, à droite de l'emplacement réservé à la signature. </p>
                                    </div>
                                </div>
                            </div>


                            <div className="button_right">
                                <Primarybutton link={link} msg={msg} href={href} color={color}>
                                </Primarybutton>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
