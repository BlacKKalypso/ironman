import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import React, { Suspense, lazy } from 'react';
import './App.css'

const Home = lazy(() => import('./route/Home'));
const Group = lazy(() => import('./route/Group'));
const Card = lazy(() => import('./route/Cards'));
const Activities = lazy(() => import('./route/Activities'));
const Summary = lazy(()=> import('./route/Summary'));
const Connexion = lazy(()=> import('./route/Connexion'));
const Payment = lazy(()=> import('./route/Payment'));
const Validation = lazy(()=> import('./route/Validation'));
const LienPartager = lazy(()=> import('./route/LienPartager'));
const SelectPayment = lazy(()=> import('./route/SelectPayment'));
const Chargement = lazy(()=> import('./route/Chargement'));
const Inscription = lazy(()=> import('./route/Inscription'));


function App() {
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/Group" component={Group}/>
          <Route exact path="/Connexion" component={Connexion}/>
          <Route exact path="/Cards" component={Card}/>
          <Route exact path="/Activities" component={Activities}/>
          <Route exact path="/Summary" component={Summary}/>
          <Route exact path="/Payment" component={Payment}/>
          <Route exact path="/Validation" component={Validation}/>
          <Route exact path="/Lien" component={LienPartager}/>
          <Route exact path="/SelectPayment" component={SelectPayment}/>
          <Route exact path="/Chargement" component={Chargement}/>
          <Route exact path="/Inscription" component={Inscription}/>
        </Switch>
      </Suspense>
    </Router>
    );
  }
  
  export default App;