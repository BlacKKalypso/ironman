import React, { PureComponent } from 'react';
import Logo from './logo_go2@2x.png';
import './header_center.css';

export default class Header extends PureComponent {
  render() {
    return (
      <header >
        <div className="header_center">
        <img alt="logo" src={Logo}></img>
        </div>
      </header>
    )
  }
}
