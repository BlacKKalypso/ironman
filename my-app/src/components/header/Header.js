import React, { PureComponent } from 'react';
import Logo from './logo_go2.png';
import './header.css';

export default class Header extends PureComponent {
  render() {
    return (
      <header>
        <div className="header_right">
          <img alt="logo" src={Logo}></img>
        </div>
      </header>
    )
  }
}
