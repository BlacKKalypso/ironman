import React, { PureComponent } from 'react'
import './pagetitle.css'

export default class Pagetitle extends PureComponent {
  render() {
    return (
      <div className="titre">
        <h1 className="pagetitle">{this.props.title}</h1>
      </div>
    )
  }
}
