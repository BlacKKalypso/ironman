import React from 'react'
import './primarybutton.css'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

function Primarybutton(props) {
  const withlink = props.link;
  const href = props.href;
  const msg = props.msg;
  const color = props.color;

  if (withlink) {
    return (
      <Link to={href} style={{color: color}}>
        <button className="primarybutton" >
          {msg}
        </button>
      </Link>  
  )
  } else {
    return (
      <button className="primarybutton" >
        {msg}
      </button>
    )
  }
}

export default Primarybutton;