import React from 'react'
import './secondarybutton.css'
import {Link} from 'react-router-dom'

function Secondarybutton(props) {
  const withlink = props.link;
  const href2 = props.href;
  const msg2 = props.msg;
  const color = props.color;

  if (withlink) {
    return (
      <Link to={href2} style={{color: color}}>
        <button className="secondarybutton" >
          {msg2}
        </button>
      </Link>  
  )
  } else {
    return (
      <button className="secondarybutton" >
        {msg2}
      </button>
    )
  }
}

export default Secondarybutton;